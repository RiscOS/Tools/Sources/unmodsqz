# Makefile for unmodsqz

COMPONENT ?= unmodsqz
INSTAPP = ${INSTDIR}${SEP}!UnModSqz

OBJS = unsqueezec unmodsqz
LIBS = ${CLXLIB}
INSTAPP_FILES = !Boot !Help !Run Desc Messages Templates \
                !Sprites:Themes !Sprites11:Themes !Sprites22:Themes \
                Morris4.!Sprites:Themes.Morris4 Morris4.!Sprites22:Themes.Morris4
INSTAPP_VERSION = Desc

include CApp

# Dynamic dependencies:
